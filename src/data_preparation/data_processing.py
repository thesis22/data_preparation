from src.config import sql_queries as queries
from src.config import config as config
import sqlite3 as sql 

_logger = config.get_logger()

def open_database(database_file):
    _logger.info(f"Connecting to database {database_file}")
    return sql.connect(database_file)
    
def get_database_cursor(database_connection):
    _logger.info(f"Fetching database cursor")
    return database_connection.cursor()

def execute_query(cursor, query, data = None):
    if data:
        _logger.info(f"Executing query: {query} with data {data}")
        cursor.execute(query, data)
    else:
        _logger.info(f"Executing query: {query}")
        cursor.execute(query)
    # TODO is there any need for batching the results?
    return cursor.fetchall()

def process_all_data_in_database(database_file):
    connection = proc.open_database(database_file)
    cursor = proc.get_database_cursor(connection)

    session_ids = proc.execute_query(cursor, queries.FETCH_SESSION_IDS)
    
    for session_id in session_ids:
        parse_session_id(cursor, session_id)

def parse_session_id(cursor, session_id):
    session_data = proc.execute_query(cursor, queries.FETCH_SESSION_TYPE, data = (session_id))[0]
    session_type = session_data
    session_type_name = proc.execute_query(cursor, queries.FETCH_SESSION_TYPE_NAME, data = (session_type))[0][0].replace(' ',"_")
    session_sequences = proc.execute_query(cursor, queries.FETCH_NUMBER_SESSION_SEQUENCES, data = (session_id))
    session_folder = os.path.join(config.DATA_FOLDER, session_type_name)
    config.create_folder_if_not_exists(session_folder)

    for session_sequence in session_sequences:
        parse_session_sequence(cursor, session_id[0], session_sequence[0],session_folder)

        
def parse_session_sequence(cursor, session_id, session_sequence, session_folder):
    session_sequence_timestamp = proc.execute_query(cursor, queries.FETCH_FIRST_TIMESTAMP_OF_SESSION_SEQUENCE, data = (session_id, session_sequence))[0]
    timestamp_string = dt.fromtimestamp(session_sequence_timestamp[0]/1000).strftime(config.datetime_format)
    filename = f"{timestamp_string}.xlsx"
    sequence_file = os.path.join(session_folder, filename)
    if os.path.isfile(sequence_file):
        _logger.warning(f"file {sequence_file} already exists, skipping")
        return

    _logger.warning(f"Creating data file {sequence_file}")

    sequence_types = proc.execute_query(cursor, queries.FETCH_SENSOR_TYPES, data = (session_id, session_sequence))

    dataframes = []

    for sequence_type in sequence_types:
        
        dataframes += parse_sensor_type(cursor, session_id, session_sequence, sequence_type[0])
    
    total_dataframe = dataframes.pop(0)
    for dataframe in dataframes:
        total_dataframe = pd.concat([total_dataframe, dataframe], axis=1)
    total_dataframe.sort_index(inplace=True)
    total_dataframe.to_excel(sequence_file)

def parse_sensor_type(cursor, session_id, session_sequence, sensor_type):
    sensor_value_indices = proc.execute_query(cursor, queries.FETCH_SENSOR_VALUE_INDICES, data = (session_id, session_sequence, sensor_type))
    sensor_type_name = proc.execute_query(cursor, queries.FETCH_SENSOR_NAME, data = (sensor_type,))
    dataframes = []
    for sensor_value_index in sensor_value_indices:        
        dataframes.append(parse_sensor_type_index(cursor, session_id, session_sequence, sensor_type, sensor_value_index[0], sensor_type_name[0][0]))
    return dataframes

def parse_sensor_type_index(cursor, session_id, session_sequence, sensor_type, value_index, sensor_type_name):
    sensor_values = proc.execute_query(cursor, queries.FETCH_SENSOR_VALUES, data = (session_id, session_sequence, sensor_type, value_index))
    timestamp = [val[0] for val in sensor_values]
    sensor_value = [val[1] for val in sensor_values]
    sensor_name = sensor_type_name+"_"+str(value_index)
    dataframe =pd.DataFrame.from_dict({
        "timestamp" : timestamp, 
        sensor_name : sensor_value
        })
    dataframe.set_index("timestamp", inplace=True)
    dataframe = dataframe[~dataframe.index.duplicated(keep='first')]
    return dataframe
