import pandas as pd
import src.config.config as config

_logger = config.get_logger()

def fill_in_data_in_file(filename):
    _logger.info(f"Loading and filling in data file {filename}")
    data = pd.read_excel(filename, engine = "openpyxl", index_col = 0)
    data = data.interpolate(method=config.interpolation_method, limit_direction='both', axis=0)
    data.to_excel(filename, engine = "openpyxl", index_label = "timestamp")