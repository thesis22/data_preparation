import src.data_preparation.data_processing as proc
import src.config.config as config
import src.config.sql_queries as queries
import src.utils.file_utils as futils
import src.data_preparation.data_filling as fill
from datetime import datetime as dt
import os
import pandas as pd

_logger = config.get_logger()

def main():
    # proc.process_all_data_in_database(config.database_file)

    datafolders = futils.find_folders_in_directory(config.DATA_FOLDER)
    
    for datafolder in datafolders:
        datafiles = futils.find_files_with_extension_in_directory(datafolder, config.data_extension)
        for datafile in datafiles:
            fill.fill_in_data_in_file(datafile)
    



if __name__ == "__main__":
    main()