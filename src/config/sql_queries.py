FETCH_SESSION_IDS = "SELECT DISTINCT session FROM sensorValues"
FETCH_SESSION_TYPE = "SELECT type FROM sensorSession WHERE id = ?"
FETCH_NUMBER_SESSION_SEQUENCES = "SELECT DISTINCT session_sequence FROM sensorValues WHERE session = ?"
FETCH_SESSION_TYPE_NAME = "SELECT name FROM sensorSessionType WHERE id = ?"
FETCH_FIRST_TIMESTAMP_OF_SESSION_SEQUENCE = "SELECT timestamp FROM sensorValues WHERE session = ? AND session_sequence = ? ORDER BY timestamp ASC LIMIT 1"

FETCH_SENSOR_TYPES = "SELECT DISTINCT type FROM sensorValues WHERE session = ? AND session_sequence = ?"
FETCH_SENSOR_VALUE_INDICES = "SELECT DISTINCT value_index FROM sensorValues WHERE session = ? AND session_sequence = ? AND type = ?"


FETCH_SENSOR_NAME = "SELECT name FROM sensors WHERE type = ?"

FETCH_SENSOR_VALUES = "SELECT timestamp, value FROM sensorValues WHERE session = ? AND session_sequence = ? AND type = ? AND value_index = ?"