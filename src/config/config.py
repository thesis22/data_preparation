import pathlib
import os
import logging
import src.config.config_logging as config_logging

PACKAGE_ROOT = pathlib.Path(config_logging.__file__).resolve().parent.parent
PACKAGE_PARENT = PACKAGE_ROOT.parent

def create_folder_if_not_exists(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

# Different logging levels can be found in config_logging.py
CONSOLE_LOGGING = True
CONSOLE_LOGGING_LEVEL = "WARNING"
FILE_LOGGING = True
FILE_LOGGING_LEVEL = "NOTSET"
LOGGING_FOLDER = PACKAGE_PARENT / "log"
VERSION_FILE_NAME = "VERSION"
VERSION_FILE = PACKAGE_ROOT / VERSION_FILE_NAME
PROJECT_NAME = "DATA_PREPARATION"

create_folder_if_not_exists(LOGGING_FOLDER)

with open(VERSION_FILE, 'r') as version_file:
    __version__ = version_file.read().strip()

# Configure logger for use in package

logger = logging.getLogger(PROJECT_NAME)
logger.setLevel(logging.DEBUG)
if CONSOLE_LOGGING:
    logger.addHandler(config_logging.get_console_handler(logging_level = CONSOLE_LOGGING_LEVEL))

logger.propagate = True
    
if FILE_LOGGING:
    logger.addHandler(config_logging.get_file_handler(PROJECT_NAME,LOGGING_FOLDER, __version__, logging_level = FILE_LOGGING_LEVEL))

def get_logger():
    return logger


DATA_FOLDER = "../data"
DATABASE_FILENAME = "SensorDataBase.db"
database_file = os.path.join(DATA_FOLDER, DATABASE_FILENAME)


datetime_format = "%Y-%m-%d_%H-%M-%S"


interpolation_method = "linear"

data_extension = "xlsx"