import logging
import sys
from datetime import datetime

# Multiple calls to logging.getLogger('someLogger') return a
# reference to the same logger object.  This is true not only
# within the same module, but also across modules as long as
# it is in the same Python interpreter process.

FORMATTER = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s -" "%(pathname)s:%(funcName)s:%(lineno)d - %(message)s"
)

LOGGING_LEVELS = {"CRITICAL": 50,
                    "ERROR": 40,
                    "WARNING": 30,
                    "INFO": 20,
                    "DEBUG": 10,
                    "NOTSET": 0}


def get_console_handler(logging_level = "NOTSET"):
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    console_handler.setLevel(LOGGING_LEVELS[logging_level])
    return console_handler

def get_file_handler(name,logging_folder, version, logging_level = "NOTSET"):
    fileLocation = f'{logging_folder}/{name}/{version}'
    import os
    if not os.path.exists(fileLocation):
        os.makedirs(fileLocation)
    
    from datetime import datetime
    now = datetime.now()
    datetimeString = now.strftime("%Y-%m-%d_%H-%M-%S")

    logFile = logging.FileHandler(f'{fileLocation}/{datetimeString}.log')
    logFile.setFormatter(FORMATTER)
    logFile.setLevel(LOGGING_LEVELS[logging_level])
    return logFile