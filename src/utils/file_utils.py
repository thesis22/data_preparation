import os 

def find_folders_in_directory(directory):
    items = [os.path.join(directory, item) for item in os.listdir(directory)]
    return [item for item in items if os.path.isdir(item)]

def find_files_with_extension_in_directory(directory, extension):
    items = [os.path.join(directory, item) for item in os.listdir(directory)]
    return [item for item in items if item.endswith(extension)]